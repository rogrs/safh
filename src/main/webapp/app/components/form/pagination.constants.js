(function() {
    'use strict';

    angular
        .module('safhApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
