
  ${AnsiColor.GREEN}███████╗ ${AnsiColor.RED}  █████╗  ${AnsiColor.BLUE} ███████╗ ${AnsiColor.YELLOW} ██╗  ██╗
  ${AnsiColor.GREEN}██╔════╝ ${AnsiColor.RED} ██╔══██╗ ${AnsiColor.BLUE} ██╔════╝ ${AnsiColor.YELLOW} ██║  ██║
  ${AnsiColor.GREEN}███████╗ ${AnsiColor.RED} ███████║ ${AnsiColor.BLUE} █████╗   ${AnsiColor.YELLOW} ███████║
  ${AnsiColor.GREEN}╚════██║ ${AnsiColor.RED} ██╔══██║ ${AnsiColor.BLUE} ██╔══╝   ${AnsiColor.YELLOW} ██╔══██║
  ${AnsiColor.GREEN}███████║ ${AnsiColor.RED} ██║  ██║ ${AnsiColor.BLUE} ██║      ${AnsiColor.YELLOW} ██║  ██║
  ${AnsiColor.GREEN}╚══════╝ ${AnsiColor.RED} ╚═╝  ╚═╝ ${AnsiColor.BLUE} ╚═╝      ${AnsiColor.YELLOW} ╚═╝  ╚═╝


${AnsiColor.BRIGHT_BLUE}:: SAFH 🤓  :: Running Spring Boot ${spring-boot.version} ::
:: http://52.67.61.182:8080 ::${AnsiColor.DEFAULT}
