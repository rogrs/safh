/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package br.com.rogrs.safh.web.rest.dto;
